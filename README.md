# presentations
JavaScript presentations made with [presentr](https://github.com/Simonwep/presentr).

## License
Code is licensed under The Unlicense, Content is licensed under CC-BY-4.0